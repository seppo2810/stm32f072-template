/**
 * @file
 *
 *****************************************************************************
 * @title   spi_dma.c
 * @author  Daniel Schnell (deexschs)
 * @brief
 *****************************************************************************
 */

/* Includes ------------------------------------------------------------------*/

#include <stdbool.h>

#include "stm32f0xx_spi.h"
#include "stm32f0xx_misc.h"

#include "FreeRTOS.h"
#include "task.h"

#include "spi_dma.h"
#include "spi_priv.h"
#include "spi_host_com.h"

#ifdef SPI_USES_DMA

static char spi_tx_dummy[1] =
{ '\0' };
static bool spi_dma_error = false;

/* Function definitions */

/**
 * Set up SPI Rx DMA activity.
 *
 * @param   spi     The spi to use. Can be either spi1 or spi2.
 * @param   addr    Destination address to transfer bytes from SPI to. This address must not be
 *                  stack based and has to be allocated from heap.
 *                  It should be aligned to 32 bit.
 * @param   len     The number of bytes to transfer from addr over SPI.
 *                  It must not exceed 65535 bytes
 */
void spi_dma_rx_init(DiagSPI* spi, void* addr, uint16_t len)
{
    DMA_DeInit(spi->dma_channel_rx);
    DMA_InitTypeDef DMA_InitStructure;
    DMA_StructInit(&DMA_InitStructure);

    DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) spi->dma_addr;

    /* DMA channel Rx of SPI Configuration */
    if ((spi->transfer_buf_ptr != NULL )&&
            (spi->transfer_buf_size != 0))
    {
        DMA_InitStructure.DMA_BufferSize = len + spi->transfer_buf_size;
    }
    else
    {
        DMA_InitStructure.DMA_BufferSize = len;
    }

    DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t) addr;
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
    DMA_InitStructure.DMA_Priority = DMA_Priority_High;
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    DMA_Init(spi->dma_channel_rx, &DMA_InitStructure);

    /* read empty rx buffer */
    spi_rx_fifo_clear(spi);

    /* enable dma rx */
    spi_dma_rx_enable(spi, true);

    if (spi->is_master)
    {
        DMA_DeInit(spi->dma_channel_tx);
        /* DMA channel Tx of SPI Configuration */
        /* we send dummy bytes over SPI MOSI to trigger read from SPI slave */
        if ((spi->transfer_buf_ptr != NULL) && (spi->transfer_buf_size != 0))
        {
            /* if we have a user provided transfer buffer, we use it
             * otherwise we use  a generic dummy byte
             */
            DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t) spi->transfer_buf_ptr;
            DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
            DMA_InitStructure.DMA_BufferSize = len + spi->transfer_buf_size;
            DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
        }
        else
        {
            DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t) &spi_tx_dummy[0];
            DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Disable;
            DMA_InitStructure.DMA_BufferSize = len;
            DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
        }

        DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;
        DMA_InitStructure.DMA_Priority = DMA_Priority_Medium;
        DMA_Init(spi->dma_channel_tx, &DMA_InitStructure);

        /* Enable DMA but do not enable interrupts. We let
         * Rx interrupt handle completion and risk that Tx
         * DMA is disabled if underrun occurs (we don't care in our
         * case of half duplex mode)
         */
        DMA_ITConfig(spi->dma_channel_tx, DMA_IT_TC, DISABLE);
        DMA_Cmd(spi->dma_channel_tx, ENABLE);
        SPI_I2S_DMACmd(spi->SPIx, SPI_I2S_DMAReq_Tx, ENABLE);
    }

}

/**
 * Set up SPI Tx DMA activity.
 *
 * @param   spi     The spi to use. Can be either spi1 or spi2.
 * @param   addr    Source address to transfer bytes to SPI. This address must not be
 *                  stack based and has to be allocated from heap.
 *                  It should be aligned to 32 bit.
 * @param   len     The number of bytes to transfer from SPI to addr.
 *                  It must not exceed 65535 bytes
 */
void spi_dma_tx_init(DiagSPI* spi, const void* addr, uint16_t len)
{
    DMA_DeInit(spi->dma_channel_tx);

    DMA_InitTypeDef DMA_InitStructure;
    DMA_StructInit(&DMA_InitStructure);

    /* DMA channel Tx of SPI Configuration */
    if ((spi->transfer_buf_ptr != NULL )&& (spi->transfer_buf_size != 0))
    {
        DMA_InitStructure.DMA_BufferSize = len + spi->transfer_buf_size;
    }
    else
    {
        DMA_InitStructure.DMA_BufferSize = len;
    }
    DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) spi->dma_addr;
    DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t) addr;
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;
    DMA_InitStructure.DMA_Priority = DMA_Priority_Medium;
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;

    DMA_Init(spi->dma_channel_tx, &DMA_InitStructure);
    spi_dma_tx_enable(spi, true);
}

/**
 * Enables/Disables the DMA interrupt for Tx
 * @param  spi     The spi to use. Can be either spi1 or spi2.
 * @param  enable  true -> enables the interrupt, false -> disables the interrupt
 */
void spi_dma_tx_enable(DiagSPI* spi, bool enable)
{
    if (enable == true)
    {
        /* Enable the SPI DMA requests & all irqs*/
        DMA_ITConfig(spi->dma_channel_tx, DMA_IT_TC, ENABLE);
        DMA_Cmd(spi->dma_channel_tx, ENABLE);
        SPI_I2S_DMACmd(spi->SPIx, SPI_I2S_DMAReq_Tx, ENABLE);
    }
    else
    {
        /* Disable irqs & SPI DMA request */
        DMA_ITConfig(spi->dma_channel_tx, DMA_IT_TC, DISABLE);
        SPI_I2S_DMACmd(spi->SPIx, SPI_I2S_DMAReq_Tx, DISABLE);
        DMA_Cmd(spi->dma_channel_tx, DISABLE);
    }
}

/**
 * Enables/Disables the DMA interrupt for Rx
 * @param  spi     The spi to use. Can be either spi1 or spi2.
 * @param  enable  true -> enables the interrupt, false -> disables the interrupt
 */
void spi_dma_rx_enable(DiagSPI* spi, bool enable)
{
    if (enable == true)
    {
        /* Enable the SPI DMA requests & irq */
        DMA_ITConfig(spi->dma_channel_rx, DMA_IT_TC, ENABLE);
        DMA_Cmd(spi->dma_channel_rx, ENABLE);
        SPI_I2S_DMACmd(spi->SPIx, SPI_I2S_DMAReq_Rx, ENABLE);
    }
    else
    {
        /* Disable irqs & SPI DMA request */
        SPI_I2S_DMACmd(spi->SPIx, SPI_I2S_DMAReq_Rx, DISABLE);
        DMA_Cmd(spi->dma_channel_rx, DISABLE);
    }
}

/**
 *  We have to wait until all bytes have left TX FIFO and Chip select is inactive.
 *  DMA FIFO will be finished before TX FIFO.
 *
 * @param  spi     The spi to use. Can be either spi1 or spi2.
 *
 * @TODO : we should change behaviour to poll for idle SPI from user space not inside interrupt.
 *         Idle state has to be guaranteed before each new transaction.
 *         BETTER idea: we configure slave select as interrupt in case of slave.
 *         If IRQ occurs, we can be sure that master has completed transfer and we can signal the
 *         bottom half from there (via semaphore).
 */
static void spi_wait_tx_finished(DiagSPI* spi)
{
    /* first wait until SPI is idle, then watch out for CS */
    while (SPI_GetTransmissionFIFOStatus(spi->SPIx) != SPI_TransmissionFIFOStatus_Empty)
    {
        ;
    }

    while (SPI_I2S_GetFlagStatus(spi->SPIx, SPI_I2S_FLAG_BSY) == SET)
    {
        ;
    }
}

/**
 * Handle DMA SPI rx interrupt
 * @param  spi     The spi to use. Can be either spi1 or spi2.
 * @param  it_status The status of the device
 * @param  xHigherPriorityTaskWoken - pdFalse - no waiting time, pdTrue - waiting
 */
static void spi_dma_handle_rx_irq(DiagSPI* spi, uint32_t it_status,
        long* xHigherPriorityTaskWoken)
{
    /* transfer complete interrupt */
    if (((DMA1_IT_TC4 & it_status) != 0) || ((DMA1_IT_TC2 & it_status) != 0))
    {
        if (true == spi->rx_in_use)
        {
            // read actual counter of DMA_CNDTR
            uint16_t cnt = DMA_GetCurrDataCounter(spi->dma_channel_rx);
            spi->rx_pos = spi->rx_cnt - cnt;

            if ((spi->rx_cnt != 0) && (spi->rx_pos >= spi->rx_cnt))
            {
                /* disable Rx DMA & CS */
                spi_dma_rx_enable(spi, false);

                if (false == spi->is_master)
                {
                    /* unset Slave RDY signal */
                    spi_host_com_set_imx_request_passive();
                }

                /* signal user space task that we are finished */
                xSemaphoreGiveFromISR(spi->rx_sem, xHigherPriorityTaskWoken);
            }
            else
            {
                /* we don't like this, i.e. we have received less than expected */
                spi_dma_error = true;
            }
        }
        else
        {
            /* ignore this scenario. This happens. e.g in full duplex mode */
            spi_dma_error = true;
        }
    }
}

/**
 * Handle DMA SPI tx interrupt
 *
 * @note
 * In case we are SPI Master all tx interrupt handling is done here.
 * In case we are SPI slave and all data has been transfered via DMA,
 * we must not poll the SPI status handling with spi_wait_tx_finished(),
 * because the remote SPI master could stop its clock anytime
 * so that SPI would never finish its BUSY state => endless loop.
 * In that case we have to enable SPI tx interrupt handling and give up
 * control to SPI tx interrupt routine and signal the user space semaphore
 * from there when all bytes have left SPI tx FIFO.
 *
 * @param  spi     The spi to use. Can be either spi1 or spi2.
 * @param  it_status The status of the device
 * @param  xHigherPriorityTaskWoken - pdFalse - no waiting time, pdTrue - waiting
 */
static void spi_dma_handle_tx_irq(DiagSPI* spi, uint32_t it_status,
        long* xHigherPriorityTaskWoken)
{
    /* transfer complete or half transfer interrupt */
    if (((DMA1_IT_TC5 & it_status) != 0) || ((DMA1_IT_TC3 & it_status) != 0))
    {
        if (true == spi->tx_in_use)
        {
            uint16_t cnt = DMA_GetCurrDataCounter(spi->dma_channel_tx);
            spi->tx_pos = spi->tx_cnt - cnt;

            // Transmit finished ?
            if ((spi->tx_cnt != 0) && (spi->tx_pos >= spi->tx_cnt))
            {
                // disable DMA channel
                spi_dma_tx_enable(spi, false);
                spi_wait_tx_finished(spi);

                if (spi->is_master)
                {
                    if (false == spi->is_partial_transfer)
                    {
                        /* a partial transfer holds the chip select line enabled */
                        spi_chip_deselect(spi->SPIx);
                    }
                }
                else
                {
                    /* unset Slave RDY signal */
                    spi_host_com_set_imx_request_passive();
                }

                xSemaphoreGiveFromISR(spi->tx_sem, xHigherPriorityTaskWoken);
            }
            else
            {
                /* we don't like this */
                spi_dma_error = true;
            }
        }
        else
        {
            /* Ignore case for spi->rx_in_use. It's handled in Rx ISR */
        }
    }
    else if ((DMA1_IT_TE5 & it_status) != 0)
    {
        // error occured, what now ?
    }
}



static void spi_dma_handle_err_irq(DiagSPI* spi, uint32_t it_status,
                                    long* xHigherPriorityTaskWoken)
{
     if (it_status & DMA1_IT_TE3)
     {
         DMA_ClearITPendingBit(DMA1_IT_TE3);
     }

     if (it_status & DMA1_IT_TE2 )
     {
         DMA_ClearITPendingBit(DMA1_IT_TE2);
     }

     if (it_status & DMA1_IT_TE5 )
     {
         DMA_ClearITPendingBit(DMA1_IT_TE5);
     }

     if (it_status & DMA1_IT_TE4 )
     {
         DMA_ClearITPendingBit(DMA1_IT_TE4);
     }
}

/**
 * SPI DMA IRQ main routine
 * @param  spi     The spi to use. Can be either spi1 or spi2.
 */
static void SPI_DMA_IRQ(DiagSPI* spi)
{
    long xHigherPriorityTaskWoken = pdFALSE;
    /*
     DMA1_IT_GL2: DMA1 Channel2 global interrupt.
     DMA1_IT_TC2: DMA1 Channel2 transfer complete interrupt.
     DMA1_IT_HT2: DMA1 Channel2 half transfer interrupt.
     DMA1_IT_TE2: DMA1 Channel2 transfer error interrupt.

     DMA1_IT_GL3: DMA1 Channel3 global interrupt.
     DMA1_IT_TC3: DMA1 Channel3 transfer complete interrupt.
     DMA1_IT_HT3: DMA1 Channel3 half transfer interrupt.
     DMA1_IT_TE3: DMA1 Channel3 transfer error interrupt.

     DMA1_IT_GL4: DMA1 Channel4 global interrupt.
     DMA1_IT_TC4: DMA1 Channel4 transfer complete interrupt.
     DMA1_IT_HT4: DMA1 Channel4 half transfer interrupt.
     DMA1_IT_TE4: DMA1 Channel4 transfer error interrupt.

     DMA1_IT_GL5: DMA1 Channel5 global interrupt.
     DMA1_IT_TC5: DMA1 Channel5 transfer complete interrupt.
     DMA1_IT_HT5: DMA1 Channel5 half transfer interrupt.
     DMA1_IT_TE5: DMA1 Channel5 transfer error interrupt.
     */

    /* read all bits from ISR at once */

    uint32_t it_status = DMA1 ->ISR;
    if (it_status & DMA1_IT_TC3 )
    {
        DMA_ClearITPendingBit(DMA1_IT_TC3 | DMA1_IT_GL3 | DMA1_IT_HT3 );
        spi_dma_handle_tx_irq(spi, it_status, &xHigherPriorityTaskWoken);
    }

    it_status = DMA1 ->ISR;
    if (it_status & DMA1_IT_TC2 )
    {
        DMA_ClearITPendingBit(DMA1_IT_TC2 | DMA1_IT_GL2 | DMA1_IT_HT2 );
        spi_dma_handle_rx_irq(spi, it_status, &xHigherPriorityTaskWoken);
    }

    it_status = DMA1 ->ISR;
    if (it_status & DMA1_IT_TC5 )
    {
        DMA_ClearITPendingBit(DMA1_IT_TC5 | DMA1_IT_GL5 | DMA1_IT_HT5 );
        spi_dma_handle_tx_irq(spi, it_status, &xHigherPriorityTaskWoken);
    }

    it_status = DMA1 ->ISR;
    if (it_status & DMA1_IT_TC4 )
    {
        DMA_ClearITPendingBit(DMA1_IT_TC4 | DMA1_IT_GL4 | DMA1_IT_HT4 );
        spi_dma_handle_rx_irq(spi, it_status, &xHigherPriorityTaskWoken);
    }

    it_status = DMA1 ->ISR;
    if (it_status & (DMA1_IT_TE2|DMA1_IT_TE3|DMA1_IT_TE4|DMA1_IT_TE5) )
    {
        spi_dma_handle_err_irq(spi, it_status, &xHigherPriorityTaskWoken);
    }

    if (xHigherPriorityTaskWoken != pdFALSE)
    {
        taskYIELD();
    }
}

/**
 * Initializes DMA IRQ at interrupt controller (NVIC)
 * @param  spi     The spi to use. Can be either spi1 or spi2.
 */
void spi_dma_irq_init(DiagSPI* spi)
{
    NVIC_InitTypeDef NVIC_InitStructure;

    /* Configure the SPI interrupt priority */
    NVIC_InitStructure.NVIC_IRQChannel = spi->NVIC_IRQChannel;
    NVIC_InitStructure.NVIC_IRQChannelPriority = spi->NVIC_IRQChannelPrio;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}

/**
 * IRQ handler for DMA channel 4+5.
 *
 * This IRQ Handler overwrites the global weak symbol DMA1_Channel4_5_IRQHandler in startup_stm32f0xx.S
 * or in case STM32F072X is defined weak symbol DMA1_Channel4_5_6_7_IRQHandler in startup_stm32f072.s
 */
#ifdef STM32F072X
void DMA1_Channel4_5_6_7_IRQHandler(void)
{
    // XXX DS: Check if we really only serve SPI DMA IRQ's here ...
#else
void DMA1_Channel4_5_IRQHandler(void)
{
#endif

    SPI_DMA_IRQ(spi2);
}

/**
 * IRQ handler for DMA channel 2+3.
 *
 * This IRQ Handler overwrites the global weak symbol DMA1_Channel2_3_IRQHandler in startup_stm32f0xx.S
 */
void DMA1_Channel2_3_IRQHandler(void)
{
    SPI_DMA_IRQ(spi1);
}

#endif

/* EOF */

